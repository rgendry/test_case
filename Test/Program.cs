﻿using System;

namespace Test
{
    class Area
    {
        public void FindArea(double radius)
        {
            Console.WriteLine("Circle area is: " + Math.Pow(radius, 2) * Math.PI);
        }
        public void FindArea(double a, double b, double c)
        {
            double p = (a + b + c) / 2;
            Console.WriteLine("Triangle area is: " + Math.Sqrt(p * (p - a) * (p - b) * (p - c)));
        }
    }
    class MainClass
    {
        public static void Main(string[] args)
        {
            Area S = new Area();
            Console.WriteLine("1 - Circle, 2 - Triangle");
            int figure = int.Parse(Console.ReadLine());
            if (figure == 1)
            {
                Console.WriteLine("Enter the radius: ");
                S.FindArea(double.Parse(Console.ReadLine()));
            }
            if (figure == 2)
            {
                Console.WriteLine("Enter the first side: ");
                double a = double.Parse(Console.ReadLine());
                Console.WriteLine("Enter the second side: ");
                double b = double.Parse(Console.ReadLine());
                Console.WriteLine("Enter the third side: ");
                double c = double.Parse(Console.ReadLine());
                S.FindArea(a, b, c);
            }
        }
    }
}
